import sys

import pandas as pd

df = pd.read_csv(sys.argv[1])

print('Columns:', df.columns.values)
date_col = input('Which column is the date column?: ')
df.index = pd.to_datetime(df[date_col])
col = input('Which column to analyze?: ')
df = df[[col]]

df.iloc[40] = 20
df.iloc[60] = 70
df.iloc[62] = -40
df.iloc[100] = 10
df.iloc[250] = 45

df.to_csv(sys.argv[2])
