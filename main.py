import sys
import os

import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns

from filters import iqr_filter
from filters import z_score_filter
from missing import handle_missing_cli

if len(sys.argv) != 3:
    print('Invalid number of arguments')
    sys.exit(1)

filename, file_ext = os.path.splitext(sys.argv[1])
missing_values = ['n/a', 'na']

# Check if the file has proper type
if file_ext == '.csv':
    df = pd.read_csv(sys.argv[1], na_values=missing_values)
elif (file_ext == '.xls') | (file_ext == '.xlsx'):
    df = pd.read_excel(sys.argv[1], na_values=missing_values)
else:
    sys.exit('Invalid file type, supported types: csv, xls, xlsx')

# Remove Unnamed columns
df = df.loc[:, ~df.columns.str.contains('^Unnamed')]
print('Columns:', df.columns.values)

date_col = input('Which column is the date column?: ')
df.index = pd.to_datetime(df[date_col])
col = input('Which column to analyze?: ')
df = df[[col]]

# general cleanup
handle_missing_cli(df)

# Show the time series before choosing cleaning method
plt.figure()
plt.subplots_adjust(left=0.2, hspace=0.5)

plt.subplot(2, 1, 1)
plt.plot(df)
plt.xlabel('Date')
plt.ylabel('Data')
plt.xticks(rotation=90)
plt.title('Time series plot')

plt.subplot(2, 1, 2)
sns.boxplot(df)
plt.title('Boxplot for time series')

plt.show()

# Get outlier detection from the user
list_outlier_methods = ['z', 'i']

while True:
    outlier_method = input('What method for outlier detection? (z for Z-score, i for IQR): ')

    if outlier_method in list_outlier_methods:
        break

window = int(input('Size of sliding window: '))
# df_mean = df.rolling(window=window, center=True).mean()
# df_median = df.rolling(window=window, center=True).median()

df_cleaned = pd.DataFrame()

if outlier_method == 'z':
    df_cleaned = z_score_filter(window, df, col, 3)
    plt.figure()
    plt.plot(df)
    plt.plot(df_cleaned)
    plt.xticks(rotation=90)
    plt.show()
if outlier_method == 'i':
    df_cleaned = iqr_filter(window, df, col)
    plt.figure()
    plt.plot(df)
    plt.plot(df_cleaned)
    plt.xticks(rotation=90)
    plt.show()

# Save cleaned time series to a file

if file_ext == '.csv':
    df_cleaned.to_csv(sys.argv[2])
elif (file_ext == '.xls') | (file_ext == '.xlsx'):
    df_cleaned.to_excel(sys.argv[2])
