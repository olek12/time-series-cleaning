from functools import reduce

import pandas as pd


def join_dfs(df_list, on):
    return reduce(lambda left, right: pd.
                  merge(left, right, on=on, how='outer'),
                  df_list).dropna()
