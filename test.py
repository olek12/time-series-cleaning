import matplotlib
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

from date_as_index import index_to_date
from join_dfs import join_dfs

matplotlib.use('TkAgg')

dow = pd.read_csv('data/Dow Jones Industrial Average DJI.csv')
unemp = pd.read_csv('data/Civilian Unemployment Rate UNRATE.csv')
oil = pd.read_csv('data/Crude Oil Prices MCOILBRENTEU.csv')
hstarts = pd.read_csv('data/Housing Starts HOUST.csv')
cars = pd.read_csv('data/Total Vehicle SalesTOTALSA.csv')
retail = pd.read_csv('data/Advance Retail Sales_RSXFS.csv')
fedrate = pd.read_csv('data/Federal Interest Rates FEDFUNDS.csv')
umcsi = pd.read_excel('data/consumer_sent_UMCH_tbmics.xls', header=3)

df_list = [dow, unemp, oil, hstarts,
           cars, retail, fedrate, umcsi]

dow = dow.rename(columns={'Date': 'DATE'})

umcsi = umcsi.drop(umcsi.columns[1], axis=1)
umcsi = umcsi.dropna().reset_index()
umcsi = umcsi.drop(umcsi.columns[0], axis=1)
umcsi['Year'] = pd.to_numeric(umcsi['Year'], downcast='integer')
umcsi = umcsi.rename(columns={'Year': 'DATE',
                              'INDEX OF CONSUMER SENTIMENT': 'SENTIMENT'})
umcsi['DATE'] = pd.to_datetime(umcsi['DATE'].map(str) + '-' +
                               '06' + '-' + '30')
df_list = [unemp, oil,
           cars, fedrate, umcsi]

# for df in df_list:
#     print(df)

index_to_date(df_list, 'DATE')
new_df = join_dfs(df_list, 'DATE')
# print('\n\n' + 'After cleaning:' + '\n\n')

# for df in df_list:
#     print(df)

# print(new_df)
new_df_missing = new_df.copy()
new_df_missing[(np.random.random(new_df.shape) < 0.3)] = np.nan
fixed_new_df_missing = new_df_missing.fillna(method='ffill')
# print(new_df)
fig = plt.figure()

for column in new_df.columns:
    plt.plot(new_df.index, new_df[column], label=column)

plt.legend()
fig2 = plt.figure()

for column in fixed_new_df_missing.columns:
    plt.plot(fixed_new_df_missing.index, fixed_new_df_missing[column],
             label=column)

plt.legend()
plt.title('Fixed time series')
plt.show()
