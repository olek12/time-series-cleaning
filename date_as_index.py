import pandas as pd


def index_to_date(df_list, date_col, resample=True):
    for i, df in enumerate(df_list):
        df[date_col] = pd.to_datetime(df[date_col])
        df.index = df[date_col]
        idx_of_date_col = df.columns.get_loc(date_col)
        df.drop(df.columns[idx_of_date_col], axis=1, inplace=True)

        if resample:
            df_list[i] = df.resample('Y').mean()
            df_list[i].index = df_list[i].index.year
