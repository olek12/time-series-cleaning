# Time series cleaning program

![main window view](main%20window%20view.png "Main window view")

This is a program for time series cleaning. It can handle missing values and outliers. For missing values it offers:
- forward fill
- dropping
- interpolation

For outlier handling it provides two methods:
- z-score filtering
- iqr filtering

## Usage

1. Load the data with "Load data button" (supported file types: csv, xls, xlsx)
2. Choose which column in the file is the time column and which is the actual data
3. Setup data with "Setup data" button

You can see how your data looks like with "Show data" button:

[comment]: <> (![show data before]&#40;show%20data%20before.png "Original data"&#41;)

![show data before](show%20data%20before%200.25.png "Original data")

[Data source](https://finance.yahoo.com/quote/%5EIXIC/history?p=%5EIXIC)

For handling missing values you can use "Handle missing method" section on the left and for outliers "Handle outliers method" section.

You can compare transformed data to the original with "Compare plots" button.

For saving the transformed data to a file use "Save data" button.