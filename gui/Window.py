from PyQt5.QtCore import Qt
from PyQt5.QtGui import QPainter, QPen
from PyQt5.QtWidgets import QPushButton, QLabel, QLineEdit, \
    QListWidget, QMainWindow

from gui.gui_adapter import *
from gui.plots import *


class MainWindow(QMainWindow):
    df = pd.DataFrame()
    df_org = pd.DataFrame()
    col = int()
    x = 40
    y = 20

    def __init__(self):
        super(MainWindow, self).__init__()

        self.title = 'Time series cleaning'
        self.setWindowTitle(self.title)
        self.setGeometry(300, 300, 510, 600)

        # Window contents
        self.bt_load_data = QPushButton(self)
        self.lb_columns = QLabel(self)
        self.lst_columns = QListWidget(self)
        self.lb_columns_to_analyze = QLabel(self)
        self.lst_columns_to_analyze = QListWidget(self)
        self.bt_setup_df = QPushButton(self)
        self.bt_show_org = QPushButton(self)
        self.bt_show_curr = QPushButton(self)
        self.lb_mis_method = QLabel(self)
        self.bt_ffill_mis_data = QPushButton(self)
        self.bt_drop_mis_data = QPushButton(self)
        self.bt_interpolate = QPushButton(self)
        self.lb_outiers_method = QLabel(self)
        self.lb_window = QLabel(self)
        self.tb_window = QLineEdit(self)
        self.bt_zscore = QPushButton(self)
        self.bt_iqr = QPushButton(self)
        self.bt_compare = QPushButton(self)
        self.bt_save_data = QPushButton(self)

        self.init_ui()

        self.show()

    def init_ui(self):
        # Choose a file and load data button
        self.bt_load_data.setText('Load data')
        self.bt_load_data.clicked.connect(self.load_data)
        self.bt_load_data.setGeometry(self.x, self.y, 100, 40)

        self.lb_columns.setText('Choose date column')
        self.lb_columns.setGeometry(self.x, self.y + 70, 200, 30)

        self.lst_columns.setGeometry(self.x, self.y + 100, 200, 100)

        self.lb_columns_to_analyze.setText('Choose col. to analyze')
        self.lb_columns_to_analyze.setGeometry(self.x + 230, self.y + 70, 200, 30)

        self.lst_columns_to_analyze.setGeometry(self.x + 230, self.y + 100, 200, 100)

        # Setup dataframe button
        self.bt_setup_df.setText('Setup data')
        self.bt_setup_df.clicked.connect(self.df_setup)
        self.bt_setup_df.setGeometry(self.x, self.y + 210, 100, 40)

        # Show plot from original data
        self.bt_show_org.setText('Show original\ndata')
        self.bt_show_org.clicked.connect(self.show_org_data)
        self.bt_show_org.setGeometry(self.x + 120, self.y + 210, 130, 60)

        # Show plot from current data
        self.bt_show_curr.setText('Show data')
        self.bt_show_curr.clicked.connect(self.show_curr_data)
        self.bt_show_curr.setGeometry(self.x + 270, self.y + 210, 100, 40)

        # Handle missing section
        self.lb_mis_method.setText('Handle missing method')
        self.lb_mis_method.setGeometry(self.x, self.y + 320, 200, 30)

        self.bt_ffill_mis_data.setText('ffill')
        self.bt_ffill_mis_data.clicked.connect(self.ffill)
        self.bt_ffill_mis_data.setGeometry(self.x, self.y + 350, 50, 40)

        self.bt_drop_mis_data.setText('drop')
        self.bt_drop_mis_data.clicked.connect(self.drop)
        self.bt_drop_mis_data.setGeometry(self.x + 60, self.y + 350, 50, 40)

        self.bt_interpolate.setText('inter.')
        self.bt_interpolate.clicked.connect(self.interpolate)
        self.bt_interpolate.setGeometry(self.x+120, self.y+350, 50, 40)

        # Outliers handling section
        self.lb_outiers_method.setText('Handle outliers method')
        self.lb_outiers_method.setGeometry(self.x + 250, self.y + 320, 200, 30)

        self.lb_window.setText('Window size\nfor z-score')
        self.lb_window.setGeometry(self.x + 250, self.y + 360, 200, 40)

        self.tb_window.setGeometry(self.x + 360, self.y + 360, 50, 30)

        self.bt_zscore.setText('z-score filter')
        self.bt_zscore.clicked.connect(self.handle_zscore)
        self.bt_zscore.setGeometry(self.x + 250, self.y + 410, 130, 40)

        self.bt_iqr.setText('iqr filter')
        self.bt_iqr.clicked.connect(self.handle_iqr)
        self.bt_iqr.setGeometry(self.x + 250, self.y + 460, 130, 40)

        self.bt_compare.setText('Compare plots')
        self.bt_compare.clicked.connect(self.compare_plots)
        self.bt_compare.setGeometry(self.x, self.y + 450, 130, 40)

        # Save to file
        self.bt_save_data.setText('Save data')
        self.bt_save_data.clicked.connect(self.save_data)
        self.bt_save_data.setGeometry(self.x, self.y + 500, 100, 40)

    def paintEvent(self, event):
        painter = QPainter(self)
        painter.setPen(QPen(Qt.black, 2, Qt.SolidLine))
        painter.drawRect(self.x-20, self.y+60, 470, 230)
        painter.drawRect(self.x-20, self.y+310, 230, 100)
        painter.drawRect(self.x-20, self.y+430, 230, 130)
        painter.drawRect(self.x+230, self.y+310, 220, 210)

    def compare_plots(self):
        compare_plots(self)

    def handle_zscore(self):
        handle_zscore(self)

    def handle_iqr(self):
        handle_iqr(self)

    def load_data(self):
        load_data(self)

    def save_data(self):
        save_data(self)

    def df_setup(self):
        df_setup(self)

    def ffill(self):
        ffill(self)

    def drop(self):
        drop(self)

    def interpolate(self):
        interpolate(self)

    def show_org_data(self):
        show_org_data(self)

    def show_curr_data(self):
        show_curr_data(self)
