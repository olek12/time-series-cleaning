import matplotlib.pyplot as plt
import seaborn as sns


def show_org_data(main_win):
    if len(main_win.df_org.columns) > 1:
        return

    fig, ax = plt.subplots(2)

    ax[0].plot(main_win.df_org)
    ax[0].set_xlabel('Date')
    ax[0].set_ylabel('Data')
    ax[0].set_title('Time series plot')
    ax[0].tick_params(labelrotation=90)

    sns.boxplot(data=main_win.df_org, orient='h', ax=ax[1])
    ax[1].set_title('Boxplot for time series')

    plt.tight_layout()
    plt.show()


def show_curr_data(main_win):
    if len(main_win.df.columns) > 1:
        return

    fig, ax = plt.subplots(2)

    ax[0].plot(main_win.df)
    ax[0].set_xlabel('Date')
    ax[0].set_ylabel('Data')
    ax[0].set_title('Time series plot')
    ax[0].tick_params(labelrotation=90)

    sns.boxplot(data=main_win.df, orient='h', ax=ax[1])
    ax[1].set_title('Boxplot for time series')

    plt.tight_layout()
    plt.show()
