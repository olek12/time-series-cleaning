import matplotlib.pyplot as plt
import pandas as pd

from filters import z_score_filter, iqr_filter
from PyQt5.QtWidgets import QFileDialog, QMessageBox
from button_actions.handle_file import load_file, save_to_file
from missing import ffill_missing, drop_missing, interpolate_missing


def compare_plots(main_win):
    fig, ax = plt.subplots()

    ax.plot(main_win.df_org, linewidth=4.0)
    ax.plot(main_win.df)
    ax.legend(['original time series',
               'time series after cleaning'])
    ax.set_xlabel('Date')
    ax.set_ylabel('Data')
    ax.set_title('Time series plot')
    ax.tick_params(labelrotation=90)

    plt.tight_layout()
    plt.show()


def handle_zscore(main_win):
    window_size = int(main_win.tb_window.text())
    main_win.df = z_score_filter(window_size, main_win.df, main_win.col, 3)
    QMessageBox.question(main_win, 'z-score', 'z-score filter applied', QMessageBox.Ok)


def handle_iqr(main_win):
    # window_size = int(main_win.tb_window.text())
    main_win.df = iqr_filter(main_win.df, main_win.col)
    QMessageBox.question(main_win, 'iqr', 'iqr filter was applied', QMessageBox.Ok)


def load_data(main_win):
    file_name, _ = QFileDialog. \
        getOpenFileName(main_win, 'Select File', 'c:\\',
                        '(*.csv *.xls *.xlsx)')
    main_win.df, cols = load_file(file_name)

    main_win.lst_columns.clear()
    main_win.lst_columns_to_analyze.clear()

    for col in cols:
        main_win.lst_columns.addItem(col)
        main_win.lst_columns_to_analyze. \
            addItem(col)


def save_data(main_win):
    file_name, _ = QFileDialog. \
        getSaveFileName(main_win, 'Choose file name', 'c:\\',
                        '(*.csv *.xls *.xlsx)')
    save_to_file(file_name, main_win.df)
    QMessageBox.question(main_win, 'Save to file', 'Time series was saved', QMessageBox.Ok)


def df_setup(main_win):
    date_col = main_win.lst_columns.selectedItems()[0].text()
    main_win.col = main_win.lst_columns_to_analyze.selectedItems()[0].text()
    main_win.df.index = pd.to_datetime(main_win.df[date_col])
    main_win.df = main_win.df[[main_win.col]]
    main_win.df_org = main_win.df.copy()
    QMessageBox.question(main_win, 'Data setup', 'Data was set up', QMessageBox.Ok)


def ffill(main_win):
    main_win.df = ffill_missing(main_win.df)
    QMessageBox.question(main_win, 'Handle missing', 'ffill filter applied', QMessageBox.Ok)


def drop(main_win):
    main_win.df = drop_missing(main_win.df)
    QMessageBox.question(main_win, 'Handle missing', 'dropped missing', QMessageBox.Ok)


def interpolate(main_win):
    main_win.df = interpolate_missing(main_win.df)
    QMessageBox.question(main_win, 'Handle missing', 'interpolation applied', QMessageBox.Ok)
