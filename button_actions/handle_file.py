import os
import sys
from pathlib import Path

import pandas as pd


def load_file(file_name):
    filename, file_ext = os.path.splitext(file_name)
    missing_values = ['n/a', 'na']

    # Check if the file has proper type
    if file_ext == '.csv':
        df = pd.read_csv(file_name, na_values=missing_values)
    elif (file_ext == '.xls') | (file_ext == '.xlsx'):
        df = pd.read_excel(file_name, na_values=missing_values)
    else:
        sys.exit('Invalid file type, supported types: csv, xls, xlsx')

    # Remove Unnamed columns
    df = df.loc[:, ~df.columns.str.contains('^Unnamed')]

    return df, df.columns.values


def save_to_file(file_name, df):
    f_name, file_ext = os.path.splitext(file_name)
    f_name = Path(f_name + file_ext)

    if file_ext == '.csv':
        # df.to_csv('C:\\Users\\Olek\\UNRATE_clean.csv')
        df.to_csv(f_name)
        # print(pd.read_csv(f_name))
    elif (file_ext == '.xls') | (file_ext == '.xlsx'):
        df.to_excel(f_name)
        print('saved to excel')
