import numpy as np


def z_score_filter(window, df, col, k_std):
    new_df = df.copy()
    n_rows = len(new_df)

    for i in range(window, n_rows - window):
        win_median = np.median \
            (new_df[col].iloc[i - window:i + window + 1])
        win_std = np.std \
            (new_df[col].iloc[i - window:i + window + 1])

        if np.abs(df[col][i] - win_median) > k_std * win_std:
            new_df[col][i] = win_median

    return new_df


def iqr_filter(df, col):
    new_df = df.copy()
    n_rows = len(new_df)

    q1 = new_df.quantile(0.25)[0]
    q3 = new_df.quantile(0.75)[0]
    iqr = q3 - q1
    mean = np.mean(new_df[col].iloc[0:n_rows - 1])

    for i in range(n_rows):
        if (new_df[col][i] < q1 - 1.5 * iqr) \
                | (new_df[col][i] > q3 + 1.5 * iqr):
            new_df[col][i] = mean

    return new_df
