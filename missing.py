import math


def handle_missing_cli(df):
    n_missing = df.isnull().sum().sum()

    if n_missing > 0:
        missing_solution = input('Missing values identified, r for remove, f for fill: ')

        while missing_solution not in ['r', 'f']:
            missing_solution = input('Missing values identified, r for remove, f for fill: ')
            continue

        if missing_solution == 'r':
            df.dropna(inplace=True)
        elif missing_solution == 'f':
            df.fillna(method='ffill', inplace=True)


def ffill_missing(df):
    n_missing = df.isnull().sum().sum()

    if n_missing > 0:
        return df.fillna(method='ffill')

    return df


def drop_missing(df):
    n_missing = df.isnull().sum().sum()

    if n_missing > 0:
        return df.dropna()

    return df


def interpolate_missing(df):
    new_df = df.copy()
    n_rows = len(new_df)
    n_missing = new_df.isnull().sum().sum()

    if n_missing > 0:
        for i in range(n_rows):
            if math.isnan(new_df.iloc[i][0]):
                j = i
                k = i

                while j - 1 >= 0 and math.isnan(new_df.iloc[j-1][0]):
                    j -= 1

                while k + 1 < n_rows and math.isnan(new_df.iloc[k+1][0]):
                    k += 1

                if j - 1 < 0 or k + 1 >= n_rows:
                    if j - 1 < 0:
                        new_df.iloc[i][0] = df.iloc[k+1][0]

                    if k + 1 >= n_rows:
                        new_df.iloc[i][0] = df.iloc[j-1][0]
                else:
                    prev_elem_idx = new_df.index[j-1]
                    miss_elem_idx = new_df.index[i]
                    next_elem_idx = new_df.index[k+1]

                    prev_elem_weigth = (next_elem_idx - miss_elem_idx) / \
                                       (next_elem_idx - prev_elem_idx)
                    next_elem_weight = (miss_elem_idx - prev_elem_idx) /\
                                       (next_elem_idx - prev_elem_idx)

                    new_df.iloc[i][0] = (prev_elem_weigth * new_df.iloc[j-1][0] +
                                         next_elem_weight * new_df.iloc[k+1][0]) / \
                                        (prev_elem_weigth + next_elem_weight)

        return new_df

    return df
